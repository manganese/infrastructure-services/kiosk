# Kiosk Daemon - Infrastructure Service

## Installation

```bash
# Clone as as root
git clone git@gitlab.com:manganese/infrastructure-services/kiosk.git /var/lib/kiosk
cd /var/lib/kiosk

# Link systemd service
sh ./scripts/service-link.sh

# ... edit configuration.env ...

# Enable systemd service
sh ./scripts/service-enable.sh

# Recommend rebooting
```

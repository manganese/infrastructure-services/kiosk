#!/bin/bash
export DISPLAY=:0.0
chromium-browser --chrome --kiosk ${KIOSK_URL} --disable-pinch --no-user-gesture-required --overscroll-history-navigation=0
